# postgresws-http-example

An example app for Postgres WebServices.

## Download

```
$ git clone --depth=1 https://bitbucket.org/justows/postgresws-http-example.git
$ cd postgresws-http-example
```

## Create table

```
$ npm run create
```

## Drop table

When example table not needed:

```
$ npm run drop
```

## Start app

```
$ postgresws start
```

## Logging

```
$ postgresws log
```

## HTTP requests

GET:

```
$ curl -i http://localhost:8080; echo
$ curl -i http://localhost:8080/hello; echo
$ curl -i http://localhost:8080/unknown; echo
```

POST:

```
$ curl -i -X POST http://localhost:8080/set/key1/value1; echo
$ curl -i http://localhost:8080/get/key1; echo
$ curl -i -X POST http://localhost:8080/set/key2/value2; echo
$ curl -i http://localhost:8080/get/key2; echo
```

PUT:

```
$ curl -i -X PUT http://localhost:8080/set/key1/value123; echo
$ curl -i http://localhost:8080/get/key1; echo
```

DELETE:

```
$ curl -i -X DELETE http://localhost:8080/delete/key1; echo
$ curl -i http://localhost:8080/get/key1; echo
$ curl -i http://localhost:8080/get/key2; echo
```
