CREATE TABLE IF NOT EXISTS kv(
  key text PRIMARY KEY,
  value text NOT NULL
);
