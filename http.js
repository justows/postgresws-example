module.exports = {
  endpoints: ["http://localhost:8080"],
  services: {
    _dflt: {
      home: {
        '/': (resp) => resp.text("hello world from /."),
        hello: (resp) => resp.text("hello world!"),

        'get/&key': (params, db, resp, done) => {
          db.query("SELECT * FROM kv WHERE key=$1", [params.key]).then(
            (rslt) => {
              if (rslt.rowCount > 0) resp.text(rslt.rows[0].value);
              else resp.status(404).text("Unknown key.");
              done();
            },

            done
          );
        },

        'set/&key/&value': {
          POST: (params, db, resp, done) => {
            db.query("INSERT INTO kv(key, value) VALUES($1, $2)", [params.key, params.value]).then(
              () => {
                resp.text("OK");
                done();
              },

              done
            );
          },

          PUT: (params, db, resp, done) => {
            db.query("UPDATE kv SET value=$1 WHERE key=$2", [params.value, params.key]).then(
              () => {
                resp.text("OK");
                done();
              },

              done
            );
          }
        },

        'delete/&key': {
          actions: {
            DELETE: (params, db, resp, log, done) => {
              log.info("Removing " + params.key);
              db.query("DELETE FROM kv WHERE key = $1", [params.key]).then(
                () => {
                  resp.text("OK");
                  done();
                },

                done
              );
            }
          }
        }
      }
    } //_dflt
  }
};
