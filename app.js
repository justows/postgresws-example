//imports
const pkg = require("./package.json");

//metadata
module.exports = {
  app: pkg.name,
  desc: pkg.description,
  version: pkg.version,
  conn: require("./conn"),
  servers: {
    http: require("./http")
  }
}
